const mongoose = require("mongoose");

const Scheme = mongoose.Schema;

const trackScheme = new Scheme({
    title:{
        type: String,
        required:true
    },
    duration:{
        type: Number,
        required:true
    },
    album:{
        type: Scheme.Types.ObjectId,
        ref: 'Album',
        required: true
    }
});

const Track = mongoose.model("Track", trackScheme);
module.exports = Track;