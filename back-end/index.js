const express = require('express');
const cors = require('cors');
const albums= require('./app/albums');
const artists= require('./app/artists');
const tracks= require('./app/tracks');
const mongoose = require("mongoose");

const app = express();
app.use(cors());
const port = 8000;

app.use(express.json());
app.use(express.static('public'));


const run = async () =>{
    await mongoose.connect('mongodb://localhost/musicMaker', {
        useNewUrlParse: true,
        useUnifiedTopology: true
    });
    app.use('/albums', albums);
    app.use('/artists', artists);
    app.use('/tracks', tracks);
    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
};
run().catch(e=>{
    console.log(e);
});


